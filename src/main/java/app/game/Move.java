/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.game;

import app.board.Field;
import app.pieces.Piece;

import java.util.ArrayList;

/**
 * Represents a single move in chess.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Move {

  private Field moveFromField;
  private Field moveToField;
  private Piece capturedPiece;
  private Piece movedPiece;
  private Piece promotedPiece;
  private boolean check;
  private boolean mate;
  private boolean fromNotation;

  /**
   * Creates a chess move.
   *
   * @param movePiece moved piece
   * @param moveToField field that piece moved to
   * @param promotedType type of new piece, if the piece has been promoted
   */
  public Move(Piece movePiece, Field moveToField, char promotedType) {
    Field moveFrom = movePiece.getPosition();
    if (moveToField.getPiece() != null) this.capturedPiece = moveToField.getPiece();
    else this.capturedPiece = null;

    Piece promotedPiece =
        promotedType != Character.MIN_VALUE
            ? Piece.newFromLetter(promotedType, movePiece.getColor())
            : null;
    boolean check;
    if (movePiece.moveTo(moveToField)) {
      moveFrom.setPiece(null);
      this.moveFromField = moveFrom;
      this.moveToField = moveToField;
      this.movedPiece = movePiece;
      if (promotedPiece != null) {
        this.moveToField.setPiece(promotedPiece);
        check = Game.isKingUnderAttackBy(promotedPiece);
      } else check = Game.isKingUnderAttackBy(movePiece);
      this.promotedPiece = promotedPiece;
      this.check = check;
      this.mate = false;
      this.fromNotation = false;
    }
  }

  /**
   * Creates a chess move from notation.
   *
   * @param moveFromFields array of fields that the piece has potentially moved from
   * @param moveToField field that the piece moved to
   * @param pieceCapture true, if the piece was captured during the move
   * @param promotionTo letter type of the new promoted piece, if any
   * @param check true, if the move is check
   * @param mate true, if the move is checkmate
   */
  public Move(
      ArrayList<Field> moveFromFields,
      Field moveToField,
      boolean pieceCapture,
      char promotionTo,
      boolean check,
      boolean mate) {

    if (pieceCapture && moveToField.getPiece() != null) this.capturedPiece = moveToField.getPiece();
    else if (!pieceCapture && moveToField.getPiece() == null) this.capturedPiece = null;

    Piece movePiece;
    Piece promotedToPiece = null;
    for (Field moveFromField : moveFromFields) {
      movePiece = moveFromField.getPiece();
      if (movePiece != null) {
        if (movePiece.moveTo(moveToField)) {
          if (promotionTo != Character.MIN_VALUE) {
            promotedToPiece = Piece.newFromLetter(promotionTo, movePiece.getColor());
            moveToField.setPiece(promotedToPiece);
          }
          moveFromField.setPiece(null);
          this.moveFromField = moveFromField;
          this.moveToField = moveToField;
          this.movedPiece = movePiece;
          this.promotedPiece = promotedToPiece;
          this.check = check;
          this.mate = mate;
          this.fromNotation = true;
          break;
        }
      }
    }
  }

  /**
   * Checks whether the moved piece is pawn and ready to promotion.
   *
   * @param piece moved piece
   * @param moveTo field that piece moved to
   * @return true, if the piece is pawn and ready to be promoted
   */
  public static boolean isPromotion(Piece piece, Field moveTo) {
    return piece.getType() == 'p'
        && ((piece.getColor() == Piece.WHITE && moveTo.getRow() == 8)
            || (piece.getColor() == Piece.BLACK && moveTo.getRow() == 1));
  }

  /**
   * Returns the field that piece moved from.
   *
   * @return field that piece moved from
   */
  public Field getMoveFromField() {
    return moveFromField;
  }

  /**
   * Returns the field that piece moved to.
   *
   * @return field that piece moved to
   */
  public Field getMoveToField() {
    return moveToField;
  }

  /**
   * Returns the piece that moved piece captured.
   *
   * @return captured piece
   */
  public Piece getCapturedPiece() {
    return capturedPiece;
  }

  /**
   * Returns the piece that was moved.
   *
   * @return moved piece
   */
  public Piece getMovedPiece() {
    return movedPiece;
  }

  /**
   * Returns the piece that has been created in promotion.
   *
   * @return promoted piece
   */
  public Piece getPromotedPiece() {
    return promotedPiece;
  }

  /**
   * Checks if the move is check in the game.
   *
   * @return true, if the move is check
   */
  boolean isCheck() {
    return check;
  }

  /** Sets checkmate to the game. */
  void setMate() {
    this.mate = true;
  }

  /**
   * Checks if the move is checkmate in the game.
   *
   * @return true, if the move is checkmate
   */
  boolean isMate() {
    return mate;
  }

  /**
   * Checks if the move was loaded from file notation.
   *
   * @return true, if the move was loaded from the file
   */
  public boolean isFromNotation() {
    return fromNotation;
  }

  /**
   * Overrides method in order to print move in correct notation format.
   *
   * @return string representation of the move in correct notation format
   */
  @Override
  public String toString() {
    String capture = this.capturedPiece != null ? "x" : "";
    String checkMate = this.mate ? "#" : (this.check ? "+" : "");
    return (this.movedPiece.getType() == 'p' ? "" : Character.toString(this.movedPiece.getType()))
        + this.moveFromField.getColumnLetter()
        + this.getMoveFromField().getRow()
        + capture
        + this.getMoveToField().getColumnLetter()
        + this.getMoveToField().getRow()
        + (this.promotedPiece != null ? this.promotedPiece.getType() : "")
        + checkMate;
  }
}
