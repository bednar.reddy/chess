/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.game;

import app.board.Board;
import app.pieces.Pawn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import app.board.Field;
import app.pieces.Piece;

import java.util.ArrayList;

/**
 * Represents chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Game {

  private Board board;
  private ObservableList<Move> moves = FXCollections.observableArrayList();
  private ObservableList<Move> displayedMoves = FXCollections.observableArrayList();
  private int currentMoveIndex;
  private boolean whiteOnTurn;
  private ArrayList<Piece> whitePieces = new ArrayList<>();
  private ArrayList<Piece> blackPieces = new ArrayList<>();

  /** Creates a new game with initialized board and pieces. */
  public Game() {
    this.board = new Board(whitePieces, blackPieces);
    this.currentMoveIndex = -1;
    this.whiteOnTurn = Piece.WHITE;
    this.setValidMovesForAll();
    System.out.println("Game initialized.");
  }

  /**
   * Checks, whether the specified piece attacks the opponents king.
   *
   * @param attacker piece potentially attacking the king
   * @return true if the specified piece is attacker
   */
  static boolean isKingUnderAttackBy(Piece attacker) {
    for (Field moveField : attacker.findPossibleNeighbourMoveFields()) {
      if (moveField.getPiece() != null && moveField.getPiece().getType() == 'K') return true;
    }
    return false;
  }

  /**
   * Returns the index of the current move in the game.
   *
   * @return number of the current move in moves array
   */
  public int getCurrentMoveIndex() {
    return this.currentMoveIndex;
  }

  /** Removes last move from the notation panel. */
  public void undoDisplayedMove() {
    this.displayedMoves.remove(this.currentMoveIndex);
  }

  /** Adds next move from moves array to the notation panel. */
  public void redoDisplayedMove() {
    this.displayedMoves.add(this.moves.get(this.currentMoveIndex));
  }

  /**
   * Returns all moves that are displayed in notation panel.
   *
   * @return displayed moves
   */
  public ObservableList<Move> getDisplayedMoves() {
    return this.displayedMoves;
  }

  /** Performs undo of move specified by current move index, decreases current move index. */
  public void undoMove() {
    Move prevMove;
    try {
      prevMove = moves.get(this.currentMoveIndex);
    } catch (IndexOutOfBoundsException e) {
      System.err.println("There are no previous moves.");
      return;
    }

    Field movedFrom = prevMove.getMoveFromField();
    Piece piece = prevMove.getMovedPiece();
    Field movedTo = prevMove.getMoveToField();
    Piece captured = prevMove.getCapturedPiece();

    if (captured != null) movedTo.setPiece(captured);
    else movedTo.setPiece(null);
    movedFrom.setPiece(piece);

    if (piece.getType() == 'p') {
      Pawn pawn = (Pawn) piece;
      if ((piece.getColor() == Piece.WHITE && movedFrom.getRow() == 2)
          || (piece.getColor() == Piece.BLACK && movedFrom.getRow() == 7))
        pawn.setAtStartPosition(true);
    }

    this.updatePieces(prevMove);
    this.whiteOnTurn = !this.whiteOnTurn;
    this.setValidMovesForAll();
    this.currentMoveIndex--;
  }

  /**
   * Performs the next move to the move specified by the current move index, increases the current
   * move index.
   */
  public void redoMove() {
    Move nextMove;
    try {
      nextMove = moves.get(++this.currentMoveIndex);
    } catch (IndexOutOfBoundsException e) {
      System.err.println("There are no more moves.");
      this.currentMoveIndex--;
      return;
    }

    Field moveFrom = nextMove.getMoveFromField();
    Field moveTo = nextMove.getMoveToField();
    Piece piece = nextMove.getMovedPiece();
    Piece promoted = nextMove.getPromotedPiece();

    moveFrom.setPiece(null);
    if (promoted != null) moveTo.setPiece(promoted);
    else moveTo.setPiece(piece);

    this.updatePieces(nextMove);
    this.whiteOnTurn = !this.whiteOnTurn;
    this.setValidMovesForAll();
  }

  /**
   * Adds players move to the game moves, sets all valid moves for opponent player and checks for
   * checkmate. If the game does not end, the opponent is on the turn.
   *
   * @param move performed move to be added to the game moves
   */
  public void addMove(Move move) {
    this.whiteOnTurn = !this.whiteOnTurn;
    this.updatePieces(move);
    this.moves.add(move);
    this.displayedMoves.add(move);
    this.setValidMovesForAll();
    if (move.isCheck())
      if (this.playerHasNoValidMoves()) {
        move.setMate();
      }
    this.currentMoveIndex++;
  }

  /**
   * Checks if the current move is checkmate.
   *
   * @return true, if the current move is checkmate
   */
  public boolean isCheckMate() {
    return this.moves.get(currentMoveIndex).isMate();
  }

  /**
   * Checks, if the current player has any valid moves.
   *
   * @return true, if current player has at least one valid move
   */
  private boolean playerHasNoValidMoves() {
    boolean noValidMoves = true;
    for (Piece piece : (this.whiteOnTurn ? this.whitePieces : this.blackPieces)) {
      if (!piece.getValidMoveFields().isEmpty()) noValidMoves = false;
    }
    return noValidMoves;
  }

  /** Removes all moves performed after the move specified by current move index. */
  public void removeFurtherMoves() {
    this.moves.remove(this.currentMoveIndex + 1, this.moves.size());
    this.displayedMoves.remove(this.currentMoveIndex + 1, this.displayedMoves.size());
  }

  /**
   * Checks, whether the white player is on turn.
   *
   * @return true, if white player is on turn
   */
  public boolean whiteIsOnTurn() {
    return whiteOnTurn;
  }

  /**
   * Returns all game moves.
   *
   * @return list of all game moves
   */
  public ObservableList<Move> getMoves() {
    return moves;
  }

  /**
   * Returns the chess board.
   *
   * @return chess board
   */
  public Board getBoard() {
    return board;
  }

  private void updatePieces(Move move) {
    Piece captured = move.getCapturedPiece();
    Piece promoted = move.getPromotedPiece();
    Piece moved = move.getMovedPiece();

    ArrayList<Piece> pieces;

    if (captured != null) {
      pieces = captured.getColor() ? this.whitePieces : this.blackPieces;
      if (pieces.contains(captured)) pieces.remove(captured);
      else pieces.add(captured);
    }

    if (promoted != null) {
      pieces = promoted.getColor() ? this.whitePieces : this.blackPieces;
      if (pieces.contains(promoted)) {
        pieces.remove(promoted);
        pieces.add(moved);
      } else {
        pieces.remove(moved);
        pieces.add(promoted);
      }
    }
  }

  private void setValidMovesForAll() {
    ArrayList<Piece> pieces = this.whiteOnTurn ? this.whitePieces : this.blackPieces;
    ArrayList<Piece> enemyAttackers;
    ArrayList<Field> possibleMoveFields, validMoveFields;
    enemyAttackers = this.findAttackers(!this.whiteOnTurn);
    for (Piece piece : pieces) {
      possibleMoveFields = piece.findPossibleNeighbourMoveFields();
      validMoveFields = this.removeDangerousMoveFields(piece, possibleMoveFields);
      if (!enemyAttackers.isEmpty() && piece.getType() != 'K')
        validMoveFields.addAll(this.findCaptureAttackerFields(enemyAttackers, possibleMoveFields));
      piece.setValidMoveFields(validMoveFields);
    }
  }

  private ArrayList<Field> removeDangerousMoveFields(
      Piece piece, ArrayList<Field> possibleMoveFields) {
    ArrayList<Field> validMoveFields = new ArrayList<>(possibleMoveFields);
    Field moveFromField = piece.getPosition();
    Piece takenPieceIfAny;
    for (Field moveToField : possibleMoveFields) {
      takenPieceIfAny = moveToField.getPiece();
      moveToField.setPiece(piece);
      moveFromField.setPiece(null);
      if (this.isKingUnderAttack()) validMoveFields.remove(moveToField);
      moveToField.setPiece(takenPieceIfAny);
      moveFromField.setPiece(piece);
    }
    return validMoveFields;
  }

  private ArrayList<Field> findCaptureAttackerFields(
      ArrayList<Piece> attackers, ArrayList<Field> possibleMoveFields) {
    ArrayList<Field> captureEnemyFields = new ArrayList<>();
    for (Field moveField : possibleMoveFields) {
      for (Piece attacker : attackers) {
        if (moveField.getPiece() != null && moveField.getPiece() == attacker)
          captureEnemyFields.add(moveField);
      }
    }
    return captureEnemyFields;
  }

  private ArrayList<Piece> findAttackers(boolean enemy) {
    ArrayList<Piece> possibleAttackers = enemy ? this.whitePieces : this.blackPieces;
    ArrayList<Piece> attackers = new ArrayList<>();
    for (Piece attacker : possibleAttackers) {
      if (isKingUnderAttackBy(attacker)) attackers.add(attacker);
    }
    return attackers;
  }

  private boolean isKingUnderAttack() {
    ArrayList<Piece> possibleAttackers = !this.whiteOnTurn ? this.whitePieces : this.blackPieces;
    for (Piece attacker : possibleAttackers) {
      for (Field moveField : attacker.findPossibleNeighbourMoveFields()) {
        if (moveField.getPiece() != null && moveField.getPiece().getType() == 'K') return true;
      }
    }
    return false;
  }
}
