/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.pieces;

import app.board.Field;

import java.util.ArrayList;

/**
 * Represents bishop as a piece in a chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Bishop extends Piece {

  /**
   * Creates new bishop piece with specified color, predefined type and icon.
   *
   * @param color true, if the color is white
   */
  Bishop(boolean color) {
    super(color);
    this.type = 'S';
    this.icon = 'b';
  }

  @Override
  public ArrayList<Field> findPossibleNeighbourMoveFields() {
    ArrayList<Field.Direction> directions =
        new ArrayList<Field.Direction>() {
          {
            add(Field.Direction.LU);
            add(Field.Direction.LD);
            add(Field.Direction.RU);
            add(Field.Direction.RD);
          }
        };
    return this.getMoveFieldsThroughNeighbours(directions);
  }
}
