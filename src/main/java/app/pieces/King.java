/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.pieces;

import app.board.Field;

import java.util.ArrayList;

/**
 * Represents king as a piece in a chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class King extends Piece {

  /**
   * Creates new king piece with specified color, predefined type and icon.
   *
   * @param color true, if the color is white
   */
  King(boolean color) {
    super(color);
    this.type = 'K';
    this.icon = 'k';
  }

  @Override
  public ArrayList<Field> findPossibleNeighbourMoveFields() {
    ArrayList<Field> validMoves = new ArrayList<>();
    Field validMove;
    for (Field.Direction d : Field.Direction.values()) {
      validMove = this.getPossibleNeighbourMoveFields(this.position, d, false);
      if (validMove != null) validMoves.add(validMove);
    }
    return validMoves;
  }
}
