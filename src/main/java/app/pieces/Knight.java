/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.pieces;

import app.board.Field;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Represents knight as a piece in a chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Knight extends Piece {

  /**
   * Creates new knight piece with specified color, predefined type and icon.
   *
   * @param color true, if the color is white
   */
  Knight(boolean color) {
    super(color);
    this.type = 'J';
    this.icon = 'n';
  }

  @Override
  public ArrayList<Field> findPossibleNeighbourMoveFields() {
    ArrayList<Field.Direction> downUp =
        new ArrayList<Field.Direction>() {
          {
            add(Field.Direction.D);
            add(Field.Direction.U);
          }
        };
    ArrayList<Field.Direction> leftRight =
        new ArrayList<Field.Direction>() {
          {
            add(Field.Direction.L);
            add(Field.Direction.R);
          }
        };
    ArrayList<Field> validMoves = this.checkLFields(leftRight, downUp);
    validMoves.addAll(this.checkLFields(downUp, leftRight));
    validMoves.removeIf(Objects::isNull);
    return validMoves;
  }

  private ArrayList<Field> checkLFields(
      ArrayList<Field.Direction> moveFields, ArrayList<Field.Direction> checkFields) {
    Field nextField;
    ArrayList<Field> validMoves = new ArrayList<>();
    for (Field.Direction moveField : moveFields) {
      if (this.position.nextField(moveField) != null
          && this.position.nextField(moveField).nextField(moveField) != null) {
        nextField = this.position.nextField(moveField).nextField(moveField);
        for (Field.Direction checkField : checkFields) {
          validMoves.add(getPossibleNeighbourMoveFields(nextField, checkField, false));
        }
      }
    }
    return validMoves;
  }
}
