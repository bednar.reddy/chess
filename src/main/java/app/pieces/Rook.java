/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.pieces;

import app.board.Field;

import java.util.ArrayList;

/**
 * Represents rook as a piece in a chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Rook extends Piece {

  /**
   * Creates new rook piece with specified color, predefined type and icon.
   *
   * @param color true, if the color is white
   */
  Rook(boolean color) {
    super(color);
    this.type = 'V';
    this.icon = 'r';
  }

  @Override
  public ArrayList<Field> findPossibleNeighbourMoveFields() {
    ArrayList<Field.Direction> directions =
        new ArrayList<Field.Direction>() {
          {
            add(Field.Direction.U);
            add(Field.Direction.D);
            add(Field.Direction.L);
            add(Field.Direction.R);
          }
        };
    return this.getMoveFieldsThroughNeighbours(directions);
  }
}
