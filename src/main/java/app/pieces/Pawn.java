/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.pieces;

import app.board.Field;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Represents pawn as a piece in a chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Pawn extends Piece {

  private boolean isAtStartPosition = true;

  /**
   * Creates new pawn piece with specified color, predefined type and icon.
   *
   * @param color true, if the color is white
   */
  Pawn(boolean color) {
    super(color);
    this.type = 'p';
    this.icon = 'p';
  }

  /**
   * Sets parameter, if the pawn is on its start position.
   *
   * @param atStartPosition true, if the pawn is on its start position
   */
  public void setAtStartPosition(boolean atStartPosition) {
    isAtStartPosition = atStartPosition;
  }

  @Override
  public boolean moveTo(Field moveToField) {
    if (this.validMoveFields.contains(moveToField)) {
      if (this.isAtStartPosition) {
        this.isAtStartPosition = false;
      }
      moveToField.setPiece(this);
      return true;
    } else return false;
  }

  @Override
  Field getPossibleNeighbourMoveFields(Field field, Field.Direction d, boolean enemyMustBePresent) {
    field = field.nextField(d);
    if (field != null) {
      if ((!enemyMustBePresent && field.getPiece() == null)
          || (enemyMustBePresent
              && field.getPiece() != null
              && field.getPiece().getColor() != this.color)) {
        return field;
      }
    }
    return null;
  }

  @Override
  public ArrayList<Field> findPossibleNeighbourMoveFields() {
    ArrayList<Field> validMoves = new ArrayList<>();
    if (this.color == WHITE) {
      validMoves.add(this.getPossibleNeighbourMoveFields(this.position, Field.Direction.U, false));
      if (this.isAtStartPosition)
        validMoves.add(
            this.getPossibleNeighbourMoveFields(
                this.position.nextField(Field.Direction.U), Field.Direction.U, false));
      validMoves.add(this.getPossibleNeighbourMoveFields(this.position, Field.Direction.LU, true));
      validMoves.add(this.getPossibleNeighbourMoveFields(this.position, Field.Direction.RU, true));
    } else {
      validMoves.add(this.getPossibleNeighbourMoveFields(this.position, Field.Direction.D, false));
      if (this.isAtStartPosition) {
        validMoves.add(
            this.getPossibleNeighbourMoveFields(
                this.position.nextField(Field.Direction.D), Field.Direction.D, false));
      }
      validMoves.add(this.getPossibleNeighbourMoveFields(this.position, Field.Direction.LD, true));
      validMoves.add(this.getPossibleNeighbourMoveFields(this.position, Field.Direction.RD, true));
    }
    validMoves.removeIf(Objects::isNull);
    return validMoves;
  }
}
