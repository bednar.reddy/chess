/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.pieces;

import app.board.Field;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Represents queen as a piece in a chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Queen extends Piece {

  /**
   * Creates new queen piece with specified color, predefined type and icon.
   *
   * @param color true, if the color is white
   */
  Queen(boolean color) {
    super(color);
    this.type = 'D';
    this.icon = 'q';
  }

  @Override
  public ArrayList<Field> findPossibleNeighbourMoveFields() {
    return this.getMoveFieldsThroughNeighbours(
        new ArrayList<>(Arrays.asList(Field.Direction.values())));
  }
}
