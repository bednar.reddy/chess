/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.pieces;

import app.board.Field;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Represents a piece in a chess game.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public abstract class Piece {

  /** Boolean representation of the white color. */
  public static final boolean WHITE = true;

  /** Boolean representation of the black color. */
  public static final boolean BLACK = false;

  /** Piece color */
  boolean color;

  /** Piece type. */
  char type;

  /** Piece icon. */
  char icon;

  /** Piece position */
  Field position;

  /** Board fields that the piece can legally go to. */
  ArrayList<Field> validMoveFields;

  /**
   * Creates a new piece with specified color.
   *
   * @param color true, if the piece should be white
   */
  public Piece(boolean color) {
    this.color = color;
  }

  /**
   * Creates new piece by its type specified by letter.
   *
   * @param letter piece type character
   * @param color true, if the piece should be white
   * @return new piece specified by letter
   */
  public static Piece newFromLetter(char letter, boolean color) {
    switch (letter) {
      case 'V':
        return new Rook(color);
      case 'J':
        return new Knight(color);
      case 'S':
        return new Bishop(color);
      case 'D':
        return new Queen(color);
      case 'K':
        return new King(color);
      case 'p':
        return new Pawn(color);
      default:
        System.err.println("Unknown pieces type: " + letter);
        return null;
    }
  }

  /**
   * Moves piece to the specified board field.
   *
   * @param moveToField target board field
   * @return true, if the move is successful
   */
  public boolean moveTo(Field moveToField) {
    if (this.validMoveFields.contains(moveToField)) {
      moveToField.setPiece(this);
      return true;
    } else return false;
  }

  /**
   * Returns fields that the piece can legally move to without checking the threat to the king.
   *
   * @return array of possibly legal board fields
   */
  public abstract ArrayList<Field> findPossibleNeighbourMoveFields();

  /**
   * Check if the piece can to move to the board field in specified direction.
   *
   * @param field origin field
   * @param d neighbourhood direction
   * @param enemyMustBePresent true, if the opponent piece must be present in order to move in
   *     specified direction
   * @return neighbour field specified by direction if the move is valid, else null
   */
  Field getPossibleNeighbourMoveFields(Field field, Field.Direction d, boolean enemyMustBePresent) {
    field = field.nextField(d);
    if (field != null) {
      if (field.getPiece() == null
          || (field.getPiece() != null && this.color != field.getPiece().getColor())) {
        return field;
      }
    }
    return null;
  }

  /**
   * Returns fields iterated in specified directions of the field neighbourhood, that the piece can
   * move to.
   *
   * @param directions specified directions of neighbourhood fields
   * @return array of board fields
   */
  ArrayList<Field> getMoveFieldsThroughNeighbours(ArrayList<Field.Direction> directions) {
    ArrayList<Field> validMoves = new ArrayList<>();
    Field neighbour, validMove;
    for (Field.Direction d : directions) {
      neighbour = this.position;
      do {
        validMove = this.getPossibleNeighbourMoveFields(neighbour, d, false);
        validMoves.add(validMove);
        if (validMove != null
            && validMove.getPiece() != null
            && validMove.getPiece().getColor() != this.color) break;
        neighbour = neighbour.nextField(d);
      } while (validMove != null);
    }
    validMoves.removeIf(Objects::isNull);
    return (ArrayList<Field>) validMoves.stream().distinct().collect(Collectors.toList());
  }

  /**
   * Returns color of the piece.
   *
   * @return true, if the color is white
   */
  public boolean getColor() {
    return color;
  }

  /**
   * Returns type of the piece.
   *
   * @return character representation of the piece type
   */
  public char getType() {
    return type;
  }

  /**
   * Returns position of the piece.
   *
   * @return field that the piece is onto
   */
  public Field getPosition() {
    return position;
  }

  /**
   * Sets the piece position.
   *
   * @param position field that the piece is onto
   */
  public void setPosition(Field position) {
    this.position = position;
  }

  /**
   * Returns fields that the piece can legally move to.
   *
   * @return array of valid move fields
   */
  public ArrayList<Field> getValidMoveFields() {
    return validMoveFields;
  }

  /**
   * Sets fields that the piece can legally move to.
   *
   * @param validMoveFields array of valid move fields
   */
  public void setValidMoveFields(ArrayList<Field> validMoveFields) {
    this.validMoveFields = validMoveFields;
  }

  /**
   * Returns name of the icon file.
   *
   * @return name of the icon file
   */
  public String getIcon() {
    return (this.getColor() == Piece.WHITE ? "w" : "b") + this.icon + ".png";
  }

  @Override
  public String toString() {
    return (this.color == Piece.WHITE ? "White " : "Black ") + this.getType();
  }
}
