/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova  <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar      <xbedna61@stud.fit.vutbr.cz>
 */
package app.board;

import app.pieces.Piece;

/**
 * Represents a field on chess board.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Field {

  /** Boolean value representing the white field. */
  public static final boolean WHITE = true;

  private boolean color;
  private int row;
  private int column;
  private Piece piece;
  private Field[] neighbourhood;

  /**
   * Creates new field with specified position, color and prepare its neighbourhood array.
   *
   * @param row number of row to be assigned with the field
   * @param column number of column to be assigned with the field
   * @param color true, if the field would be white
   */
  public Field(int row, int column, boolean color) {
    this.row = row;
    this.column = column;
    this.color = color;
    this.neighbourhood = new Field[Field.Direction.values().length];
  }

  /**
   * Returns neighbour field in specified direction.
   *
   * @param direction direction in field neighbourhood
   * @return field from neighbourhood in specified direction
   */
  public Field nextField(Direction direction) {
    return this.neighbourhood[direction.ordinal()];
  }

  /**
   * Returns color of the field.
   *
   * @return true, if the color is white
   */
  public boolean getColor() {
    return color;
  }

  /**
   * Returns row of the field.
   *
   * @return number of field row
   */
  public int getRow() {
    return row;
  }

  /**
   * Returns column of the field.
   *
   * @return character of the field column
   */
  public char getColumnLetter() {
    return (char) (column - 1 + 'a');
  }

  /**
   * Returns piece from the field.
   *
   * @return piece that is put on the field, null if the field is empty
   */
  public Piece getPiece() {
    return piece;
  }

  /**
   * Sets piece to the field and field to the piece.
   *
   * @param piece specified piece to be put on the field, or null to remove the piece from field
   */
  public void setPiece(Piece piece) {
    if (piece != null) piece.setPosition(this);
    this.piece = piece;
  }

  /**
   * Changes string representation of the field so it can be printed onto GUI board.
   *
   * @return string representation of the field position
   */
  @Override
  public String toString() {
    return Character.toString(this.getColumnLetter()) + this.getRow();
  }

  /**
   * Adds neighbour field to the neighbourhood in specified direction.
   *
   * @param d direction in which neighbour field is added
   * @param f field in neighbourhood
   */
  void addNextField(Direction d, Field f) {
    this.neighbourhood[d.ordinal()] = f;
  }

  /**
   * Gets direction from neighbourhood coordinates.
   * [LU] -  [U]  - [RU]     [-1, 1] -  [0, 1]  - [1, 1]
   * [L] - current - [R]  ~  [-1, 0] - current  - [1, 0]
   * [LD] -  [D]  - [RD]     [-1,-1] - [0, -1] - [1, -1]
   *
   * @param xLocal x coordinate in field neighbourhood
   * @param yLocal y coordinate in field neighbourhood
   * @return direction based on specified coordinates
   */
  Direction getDirection(int xLocal, int yLocal) {
    Direction d = null;
    switch (xLocal) {
      case -1:
        {
          switch (yLocal) {
            case -1:
              d = Field.Direction.LD;
              break;
            case 0:
              d = Field.Direction.L;
              break;
            case 1:
              d = Field.Direction.LU;
              break;
          }
          break;
        }
      case 0:
        {
          switch (yLocal) {
            case -1:
              d = Field.Direction.D;
              break;
            case 1:
              d = Field.Direction.U;
              break;
          }
          break;
        }
      case 1:
        {
          switch (yLocal) {
            case -1:
              d = Field.Direction.RD;
              break;
            case 0:
              d = Field.Direction.R;
              break;
            case 1:
              d = Field.Direction.RU;
              break;
          }
        }
    }
    return d;
  }

  /** Defines Moore neighbourhood directions. */
  public enum Direction {
    LD,
    L,
    LU,
    D,
    U,
    RD,
    R,
    RU
  }
}
