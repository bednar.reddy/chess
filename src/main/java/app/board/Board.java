/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.board;

import app.pieces.Piece;

import java.util.ArrayList;

/**
 * Represents a chess board.
 *
 * @author Jana Balkovicova, Radek Bednar
 */
public class Board {

  /** Number of chess board rows. */
  public static final byte ROWS_COUNT = 8;

  /** Number of chess board columns. */
  public static final byte COLS_COUNT = 8;

  private Field[][] fields;

  /**
   * Creates new board and sets an initial state.
   *
   * @param whitePieces array of white pieces to be created
   * @param blackPieces array of black pieces to be created
   */
  public Board(ArrayList<Piece> whitePieces, ArrayList<Piece> blackPieces) {
    this.fields = new Field[Board.ROWS_COUNT][Board.COLS_COUNT];
    this.generateFields();
    this.linkNeighbours();
    this.setInitialState(whitePieces, blackPieces);
  }

  /**
   * Returns board field specified by row number and column character.
   *
   * @param row number of field row
   * @param column character of field column
   * @return specified board field
   */
  public Field getField(int row, char column) {
    return this.fields[row - 1][column - 'a'];
  }

  /**
   * Returns board field specified by row number and column number.
   *
   * @param row number of field row
   * @param column number of field character
   * @return specified board field
   */
  public Field getField(int row, int column) {
    return this.fields[row - 1][column - 1];
  }

  /**
   * Returns board fields specified by chess pieces.
   *
   * @param pieceType type of desired pieces
   * @param pieceColor white, if the pieces should be white
   * @param pieceRow row of desired pieces
   * @param pieceColumn column of desired pieces
   * @return array of board fields found
   */
  public ArrayList<Field> findFieldsByPiece(
      char pieceType, boolean pieceColor, int pieceRow, char pieceColumn) {
    Field field;
    ArrayList<Field> fields = new ArrayList<>();
    if (pieceRow == 0 && pieceColumn == Character.MIN_VALUE) {
      for (int row = 1; row <= ROWS_COUNT; row++) {
        for (int column = 1; column <= COLS_COUNT; column++) {
          field = this.fields[row - 1][column - 1];
          if (field.getPiece() != null
              && field.getPiece().getType() == pieceType
              && field.getPiece().getColor() == pieceColor) fields.add(field);
        }
      }
    } else {
      for (int i = 1; i <= this.fields.length; i++) {
        if (pieceColumn == Character.MIN_VALUE) {
          field = this.getField(pieceRow, i);
        } else field = this.getField(i, pieceColumn);
        if (field.getPiece() != null
            && field.getPiece().getType() == pieceType
            && field.getPiece().getColor() == pieceColor) fields.add(field);
      }
    }
    return fields;
  }

  private void generateFields() {
    boolean color = Field.WHITE;
    for (int row = 1; row <= ROWS_COUNT; row++) {
      for (int col = 1; col <= COLS_COUNT; col++) {
        this.fields[row - 1][col - 1] = new Field(row, col, color);
        color = !color;
      }
      color = !color;
    }
  }

  private void generateNeighbours(Field current, int boardRow, int boardCol) {
    /*
    [LU] -  [U]  - [RU]     [-1, 1] -  [0, 1]  - [1, 1]
    [L] - current - [R]  ~  [-1, 0] - current  - [1, 0]
    [LD] -  [D]  - [RD]     [-1,-1] - [0, -1] - [1, -1]

    starting at [-1, -1]
    */
    int neighCol, neighRow;
    int boardMin = 1;
    int boardMax = COLS_COUNT;
    Field.Direction d;
    for (neighCol = boardCol - 1; neighCol <= boardCol + 1; neighCol++) {
      for (neighRow = boardRow - 1; neighRow <= boardRow + 1; neighRow++) {
        if (neighCol == boardCol && neighRow == boardRow) {
          continue;
        }
        d = current.getDirection(neighCol - boardCol, neighRow - boardRow);
        if (neighCol < boardMin
            || neighRow < boardMin
            || neighCol > boardMax
            || neighRow > boardMax) {
          current.addNextField(d, null);
        } else {
          current.addNextField(d, this.getField(neighRow, neighCol));
        }
      }
    }
  }

  private void linkNeighbours() {
    Field current;
    int boardCol, boardRow;
    for (boardRow = 1; boardRow <= ROWS_COUNT; boardRow++) {
      for (boardCol = 1; boardCol <= COLS_COUNT; boardCol++) {
        current = this.getField(boardRow, boardCol);
        generateNeighbours(current, boardRow, boardCol);
      }
    }
  }

  private void setInitialState(ArrayList<Piece> whitePieces, ArrayList<Piece> blackPieces) {
    char[] order = {'V', 'J', 'S', 'D', 'K', 'S', 'J', 'V'};
    Piece newPiece;
    for (int col = 1; col <= COLS_COUNT; col++) {
      Field field = this.getField(1, col);
      newPiece = Piece.newFromLetter(order[col - 1], Piece.WHITE);
      field.setPiece(newPiece);
      whitePieces.add(newPiece);
      field = this.getField(2, col);
      newPiece = Piece.newFromLetter('p', Piece.WHITE);
      field.setPiece(newPiece);
      whitePieces.add(newPiece);

      field = this.getField(8, col);
      newPiece = Piece.newFromLetter(order[col - 1], Piece.BLACK);
      field.setPiece(newPiece);
      blackPieces.add(newPiece);
      field = this.getField(7, col);
      newPiece = Piece.newFromLetter('p', Piece.BLACK);
      field.setPiece(newPiece);
      blackPieces.add(newPiece);
    }
  }
}
