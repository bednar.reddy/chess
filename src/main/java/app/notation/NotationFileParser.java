/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.notation;

import app.board.Field;
import app.errors.InvalidNotationFormat;
import app.game.Game;
import app.game.Move;
import app.pieces.Piece;

import java.io.*;
import java.util.ArrayList;

/**
 * Performs parsing notation from file.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class NotationFileParser {

  private Game game;

  private int actualLine;

  /**
   * Creates and initializes new notation file parser.
   *
   * @param game parsed moves are going to be loaded into this game
   */
  public NotationFileParser(Game game) {
    this.actualLine = 0;
    this.game = game;
  }

  /**
   * Parses notation from file, while creating moves and adding them to the game.
   *
   * @param file specified file where notation is stored
   * @throws InvalidNotationFormat thrown then notation has incorrect format
   * @throws IOException thrown when error occurs while reading or writing to the file
   */
  public void parseNotation(File file) throws InvalidNotationFormat, IOException {
    String line;

    FileReader fileReader = new FileReader(file);
    BufferedReader bufferedReader = new BufferedReader(fileReader);

    System.out.println("Notation reading started.");
    while ((line = bufferedReader.readLine()) != null) {
      this.actualLine++;

      String[] splitted = line.split("\\s+");
      if (splitted.length != 3) {
        throw new InvalidNotationFormat(
            "Each line should contain move order, white move and black move.", this.actualLine);
      }

      int moveOrder;
      try {
        moveOrder = this.getMoveOrderFromString(splitted[0]);
      } catch (NumberFormatException ex) {
        throw new InvalidNotationFormat("Move order has to be valid integer.", this.actualLine);
      }

      if (this.game.getMoves().size() / 2 + 1 != moveOrder) {
        throw new InvalidNotationFormat("Error in order numbers sequence.", this.actualLine);
      }

      this.game.addMove(this.parseMovePropertiesFromString(splitted[1], Piece.WHITE));
      this.game.addMove(this.parseMovePropertiesFromString(splitted[2], Piece.BLACK));
    }

    System.out.println("Notation reading finished without errors.");
    bufferedReader.close();
  }

  private Move parseMovePropertiesFromString(String stringMove, boolean pieceColor)
      throws InvalidNotationFormat {
    System.out.println("==========================");
    System.out.println("Parsing move: " + stringMove);

    char[] chars = stringMove.toCharArray();
    if (chars.length < 2) {
      throw new InvalidNotationFormat(
          "Move has to contain at least target field.", this.actualLine);
    }
    if (chars.length > 6) {
      throw new InvalidNotationFormat(
          "Move has to contain at most pieces type, pieces take, start field, end field and chess or mate sign.",
          this.actualLine);
    }

    char pieceType = 'p';
    char endColumn = Character.MIN_VALUE;
    char endRow = Character.MIN_VALUE;
    char startColumn = Character.MIN_VALUE;
    char startRow = Character.MIN_VALUE;
    char promotionTo = Character.MIN_VALUE;
    boolean pieceTake = false;
    boolean check = false;
    boolean mate = false;

    MovePart previousPart = MovePart.START;
    for (int i = 0; i < chars.length; i++) {
      MovePart actualPart = getMovePartFromChar(chars[i]);
      if (actualPart == MovePart.PIECE && previousPart != MovePart.START) {
        actualPart = MovePart.PROMOTION_TO;
      }

      switch (actualPart) {
        case PIECE:
          pieceType = chars[i];
          break;
        case COLUMN:
          if (endColumn != Character.MIN_VALUE) {
            startColumn = endColumn;
            endColumn = chars[i];
          } else {
            endColumn = chars[i];
          }
          break;
        case ROW:
          if (endRow != Character.MIN_VALUE) {
            startRow = endRow;
            endRow = chars[i];
          } else {
            endRow = chars[i];
          }
          break;
        case PIECE_TAKE:
          if (startColumn == Character.MIN_VALUE && startRow == Character.MIN_VALUE) {
            pieceTake = true;
          } else {
            throw new InvalidNotationFormat(
                "Piece take sign on incorrect position.", this.actualLine);
          }
          break;
        case CHECK:
          if (i == chars.length - 1) {
            check = true;
          } else {
            throw new InvalidNotationFormat("Check sign on incorrect position.", this.actualLine);
          }
          break;
        case MATE:
          if (i == chars.length - 1) {
            mate = true;
          } else {
            throw new InvalidNotationFormat("Mate sign on incorrect position.", this.actualLine);
          }
          break;
        case PROMOTION_TO:
          if (previousPart == MovePart.ROW || previousPart == MovePart.COLUMN) {
            promotionTo = chars[i];
          } else {
            throw new InvalidNotationFormat("Piece type on incorrect position.", this.actualLine);
          }
          break;
        case UNKNOWN:
          throw new InvalidNotationFormat("Unknown character in move notation.", this.actualLine);
      }

      previousPart = actualPart;
    }

    if (endColumn == Character.MIN_VALUE || endRow == Character.MIN_VALUE) {
      throw new InvalidNotationFormat("End column and end row has to be defined.", this.actualLine);
    }

    System.out.println("--------------------------");
    System.out.println("pieceType: " + pieceType);
    System.out.println("startColumn: " + (startColumn != Character.MIN_VALUE ? startColumn : "no"));
    System.out.println("startRow: " + (startRow != Character.MIN_VALUE ? startRow : "no"));
    System.out.println("pieceTake: " + (pieceTake ? "yes" : "no"));
    System.out.println("endColumn: " + endColumn);
    System.out.println("endRow: " + endRow);
    System.out.println("promotionTo: " + (promotionTo != Character.MIN_VALUE ? promotionTo : "no"));
    System.out.println("check: " + (check ? "yes" : "no"));
    System.out.println("mate: " + (mate ? "yes" : "no"));
    System.out.println("==========================");

    ArrayList<Field> startFields = new ArrayList<>();
    if (startRow != Character.MIN_VALUE && startColumn != Character.MIN_VALUE)
      startFields.add(
          this.game.getBoard().getField(Character.getNumericValue(startRow), startColumn));
    else
      startFields =
          this.game.getBoard().findFieldsByPiece(pieceType, pieceColor, startRow, startColumn);

    Field endField = this.game.getBoard().getField(Character.getNumericValue(endRow), endColumn);

    return new Move(startFields, endField, pieceTake, promotionTo, check, mate);
  }

  private int getMoveOrderFromString(String string) {
    string = string.replaceAll("\\.", "");
    return Integer.valueOf(string);
  }

  private MovePart getMovePartFromChar(char ch) {
    switch (ch) {
      case 'V':
      case 'J':
      case 'S':
      case 'D':
      case 'K':
      case 'p':
        return MovePart.PIECE;
      case 'a':
      case 'b':
      case 'c':
      case 'd':
      case 'e':
      case 'f':
      case 'g':
      case 'h':
        return MovePart.COLUMN;
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
        return MovePart.ROW;
      case 'x':
        return MovePart.PIECE_TAKE;
      case '+':
        return MovePart.CHECK;
      case '#':
        return MovePart.MATE;
    }

    return MovePart.UNKNOWN;
  }

    /**
     * Saves existing game moves in notation format to the specified file.
     *
     * @param file specified file where notation will be saved
     */
  public void saveNotation(File file) {
    FileWriter fileWriter;
    try {
      fileWriter = new FileWriter(file);
    } catch (IOException ex) {
      System.out.println("Error writing to file '" + file.getAbsolutePath() + "'");
      return;
    }

    String notation = "";
    StringBuilder stringBuilder = new StringBuilder();
    int movesCount = this.game.getMoves().size();
    int line = 1;
    for (int i = 0; i < movesCount; i++) {
      if (i % 2 == 0) {
        stringBuilder.append(line).append(". ");
        stringBuilder.append(this.game.getMoves().get(i)).append(" ");
        line++;
      } else {
        stringBuilder.append(this.game.getMoves().get(i)).append("\n");
      }
      notation = stringBuilder.toString();
    }

    try {
      fileWriter.write(notation);
      fileWriter.close();
    } catch (IOException ex) {
      System.out.println("Error writing to file '" + file.getAbsolutePath() + "'");
    }
  }

  /** Define parts of the move notation. */
  enum MovePart {
    START,
    PIECE,
    COLUMN,
    ROW,
    PIECE_TAKE,
    CHECK,
    MATE,
    PROMOTION_TO,
    UNKNOWN,
  }
}
