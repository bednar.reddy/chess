/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package app.errors;

/**
 * Indicates wrong format of chess notation.
 *
 * @author Radek Bednar
 */
public class InvalidNotationFormat extends Exception {

  /**
   * Thrown to indicate that input file has incorrect chess notation format.
   *
   * @param message additional text message to the exception
   * @param line number of line where exception was thrown
   */
  public InvalidNotationFormat(String message, int line) {
    super("Invalid notation format on line " + line + ": " + message);
  }
}
