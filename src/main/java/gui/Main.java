/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui;

import gui.layout.WindowComponent;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main GUI class.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class Main extends Application {

  /**
   * Runs the standalone application.
   *
   * @param args arguments passed to the application
   */
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    WindowComponent windowComponent = new WindowComponent(primaryStage);
    Scene scene = new Scene(windowComponent, 1250, 800);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  @Override
  public void stop() throws Exception {
    super.stop();
    System.exit(0);
  }
}
