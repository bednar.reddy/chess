/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.layout;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Represents application window.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class WindowComponent extends VBox {

  private Stage primaryStage;

  private TabsComponent tabsComponent;

  /**
   * Creates new application window with specified stage.
   *
   * @param primaryStage top level JavaFX container
   */
  public WindowComponent(Stage primaryStage) {
    this.primaryStage = primaryStage;
    this.tabsComponent = new TabsComponent();
    Button addGameButton = new Button("Add game");

    addGameButton.setOnAction(event -> this.tabsComponent.createGame());

    this.getChildren().add(addGameButton);
    this.getChildren().add(this.tabsComponent);

    this.tabsComponent.createGame();

    VBox.setMargin(this.getChildren().get(0), new Insets(10, 0, 10 ,10));
  }

  /**
   * Returns window stage.
   *
   * @return stage
   */
  Stage getPrimaryStage() {
    return this.primaryStage;
  }
}
