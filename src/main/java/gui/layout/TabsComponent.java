/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.layout;

import app.game.Game;
import gui.game.GameComponent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

/**
 * Represents tabs in the application window.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class TabsComponent extends TabPane {

  /** Creates new game in the new tab. */
  public void createGame() {
    Tab tab = new Tab("Game " + (this.getTabs().size() + 1));
    tab.setContent(new GameComponent(new Game(), this.getPrimaryStage(), this));
    this.getTabs().add(tab);
    this.getSelectionModel().select(tab);
  }

  private Stage getPrimaryStage() {
    return ((WindowComponent) this.getParent()).getPrimaryStage();
  }

  /**
   * Removes the game tab with specified game component.
   *
   * @param gameComponent specified game component to be removed with the tab
   */
  public void removeGameTab(GameComponent gameComponent) {
    Tab tabForRemove = null;
    for (Tab tab : this.getTabs()) {
      if (tab.getContent() == gameComponent) {
        tabForRemove = tab;
      }
    }

    if (tabForRemove != null) {
      this.getTabs().remove(tabForRemove);
    }
  }
}
