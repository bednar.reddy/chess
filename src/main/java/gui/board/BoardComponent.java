/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.board;

import app.board.Board;
import app.board.Field;
import app.game.Game;
import app.game.Move;
import gui.game.GameComponent;
import gui.pieces.PieceComponent;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

import java.util.concurrent.*;

/**
 * Visually represents a chess board.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class BoardComponent extends GridPane {

  private PieceComponent currentlyMovedPieceComponent;

  private ScheduledExecutorService playModeExecutor;

  /**
   * Creates visual representation of a chess board by data from backend board.
   *
   * @param board specified board with data
   */
  public BoardComponent(Board board) {
    for (int row = 1; row <= Board.ROWS_COUNT; row++) {
      for (int col = 1; col <= Board.COLS_COUNT; col++) {
        this.add(new FieldComponent(board.getField(row, col)), col - 1, Board.ROWS_COUNT - row);
      }
    }

    Label label;
    for (int col = 1; col <= Board.COLS_COUNT; col++) {
      label = new Label(Character.toString(board.getField(1, col).getColumnLetter()));
      label.setStyle("-fx-padding: 15px");
      GridPane.setValignment(label, VPos.TOP);
      this.add(label, col - 1, 8);
    }

    for (int row = 1; row <= Board.ROWS_COUNT; row++) {
      label = new Label(Integer.toString(row));
      label.setStyle("-fx-padding: 15px");
      GridPane.setHalignment(label, HPos.LEFT);
      this.add(label, 8, Board.ROWS_COUNT - row);
    }

    for (int i = 0; i < Board.ROWS_COUNT + 1; i++) {
      this.getColumnConstraints()
          .add(
              new ColumnConstraints(
                  80,
                  Control.USE_COMPUTED_SIZE,
                  Double.POSITIVE_INFINITY,
                  Priority.ALWAYS,
                  HPos.CENTER,
                  true));
      this.getRowConstraints()
          .add(
              new RowConstraints(
                  80,
                  Control.USE_COMPUTED_SIZE,
                  Double.POSITIVE_INFINITY,
                  Priority.ALWAYS,
                  VPos.CENTER,
                  true));
    }

    this.setStyle("-fx-padding: 20px");
    this.setMaxHeight(720);
    this.setMaxWidth(720);
  }

  /**
   * Returns visual representation of a board field specified by backend field.
   *
   * @param field specified board field
   * @return field component
   */
  public FieldComponent getFieldComponentForField(Field field) {
    for (Node node : this.getChildren()) {
      FieldComponent fieldComponent = (FieldComponent) node;
      if (fieldComponent.getField() == field) {
        return fieldComponent;
      }
    }

    return null;
  }

  /**
   * Returns visual representation of a currently moved piece.
   *
   * @return currently moved piece component
   */
  PieceComponent getCurrentlyMovedPieceComponent() {
    return currentlyMovedPieceComponent;
  }

  /**
   * Sets visual representation of a chess piece that has currently moved.
   *
   * @param currentlyMovedPieceComponent piece component to be set
   */
  public void setCurrentlyMovedPieceComponent(PieceComponent currentlyMovedPieceComponent) {
    this.currentlyMovedPieceComponent = currentlyMovedPieceComponent;
  }

  /**
   * Returns visual representation of a chess game with relation to the board.
   *
   * @return game component
   */
  public GameComponent getGameComponent() {
    return (GameComponent) this.getParent();
  }

  /**
   * Automatically plays the game moves with set step time, starting with the move specified by
   * current move index in backend
   *
   * @see Game#getCurrentMoveIndex()
   */
  public void runPlayingMode() {
    Game game = this.getGameComponent().getGame();
    if (game.getMoves().size() - 1 == game.getCurrentMoveIndex()) {
      for (int i = this.getGameComponent().getGame().getCurrentMoveIndex(); i >= 0; i--) {
        this.revertMove(game.getMoves().get(i));
        game.undoMove();
      }
    }

    this.playModeExecutor =
        Executors.newScheduledThreadPool(
            1,
            runnable -> {
              Thread thread = new Thread(runnable);
              thread.setDaemon(true);
              return thread;
            });

    Runnable step =
        () ->
            Platform.runLater(
                () -> {
                  if (game.getMoves().size() - 1 != game.getCurrentMoveIndex()) {
                    this.performMove(game.getMoves().get(game.getCurrentMoveIndex() + 1));
                    game.redoMove();
                  }

                  if (game.getMoves().size() - 1 == game.getCurrentMoveIndex()) {
                    this.playModeExecutor.shutdown();
                    this.playModeExecutor = null;
                    this.getGameComponent()
                        .getToolbarComponent()
                        .getMoveControlsComponent()
                        .refreshPlayModeButton();
                  }

                  this.getGameComponent()
                      .getToolbarComponent()
                      .getMoveControlsComponent()
                      .refreshButtons();
                  this.getGameComponent()
                      .getToolbarComponent()
                      .getMoveListView()
                      .getSelectionModel()
                      .select(game.getMoves().get(game.getCurrentMoveIndex()));
                });

    long period;
    long timeUnitMillis = 500;
    int sliderValue =
        (int)
            this.getGameComponent()
                .getToolbarComponent()
                .getMoveControlsComponent()
                .getTimeSliderValue();

    switch (sliderValue) {
      case 1:
        period = timeUnitMillis * 5;
        break;
      case 2:
        period = timeUnitMillis * 4;
        break;
      case 3:
        period = timeUnitMillis * 3;
        break;
      case 4:
        period = timeUnitMillis * 2;
        break;
      default:
        period = timeUnitMillis;
    }
    this.playModeExecutor.scheduleAtFixedRate(step, period, period, TimeUnit.MILLISECONDS);
  }

  /** Pauses the automatic playback of the moves. */
  public void pausePlayingMode() {
    this.playModeExecutor.shutdown();
    this.playModeExecutor = null;
  }

  /**
   * Checks, whether is the automatic playback of the moves running.
   *
   * @return true, if the playing mode is running
   */
  public boolean isPlayingModeRunning() {
    return this.playModeExecutor != null;
  }

  /**
   * Performs visual representation of a move, specified by backend move.
   *
   * @param move specified move to be performed
   */
  public void performMove(Move move) {
    BoardComponent boardComponent = this.getGameComponent().getBoardComponent();
    FieldComponent targetFieldComponent =
        boardComponent.getFieldComponentForField(move.getMoveToField());
    FieldComponent sourceFieldComponent =
        boardComponent.getFieldComponentForField(move.getMoveFromField());
    PieceComponent movedPieceComponent = sourceFieldComponent.getPieceComponent();

    if (move.getCapturedPiece() != null) {
      targetFieldComponent.getChildren().remove(targetFieldComponent.getPieceComponent());
      targetFieldComponent.setPieceComponent(null);
    }

    targetFieldComponent.getChildren().add(movedPieceComponent);
    targetFieldComponent.setPieceComponent(movedPieceComponent);

    sourceFieldComponent.getChildren().remove(movedPieceComponent);
    sourceFieldComponent.setPieceComponent(null);

    if (move.getPromotedPiece() != null) {
      targetFieldComponent.getPieceComponent().setPiece(move.getPromotedPiece());
      targetFieldComponent.getPieceComponent().refresh();
    }
  }

  /**
   * Visually reverts move specified by backend move.
   *
   * @param move specified move to be reverted
   */
  public void revertMove(Move move) {
    BoardComponent boardComponent = this.getGameComponent().getBoardComponent();
    FieldComponent sourceFieldComponent =
        boardComponent.getFieldComponentForField(move.getMoveFromField());
    FieldComponent targetFieldComponent =
        boardComponent.getFieldComponentForField(move.getMoveToField());
    PieceComponent movedPieceComponent = targetFieldComponent.getPieceComponent();

    sourceFieldComponent.getChildren().add(movedPieceComponent);
    sourceFieldComponent.setPieceComponent(movedPieceComponent);

    targetFieldComponent.getChildren().remove(targetFieldComponent.getPieceComponent());
    targetFieldComponent.setPieceComponent(null);

    if (move.getCapturedPiece() != null) {
      PieceComponent pieceComponent = new PieceComponent(move.getCapturedPiece());
      targetFieldComponent.getChildren().add(pieceComponent);
      targetFieldComponent.setPieceComponent(pieceComponent);
    }

    if (move.getPromotedPiece() != null) {
      sourceFieldComponent.getPieceComponent().setPiece(move.getMovedPiece());
      sourceFieldComponent.getPieceComponent().refresh();
    }
  }
}
