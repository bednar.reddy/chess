/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.board;

import app.board.Field;
import app.game.Move;
import gui.game.GameComponent;
import gui.pieces.PieceComponent;
import javafx.scene.Node;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;

/**
 * Visually represents a board field.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class FieldComponent extends StackPane {

  private Field field;

  private PieceComponent pieceComponent;

  /**
   * Creates visual representation of a board field by data from backend field and sets its events.
   *
   * @param field specified field with data
   */
  public FieldComponent(Field field) {
    super();
    this.field = field;

    this.setDefaultBackground();

    if (field.getPiece() != null) {
      this.pieceComponent = new PieceComponent(field.getPiece());
      this.getChildren().add(this.pieceComponent);
    }

    this.setOnDragOver(
        event -> {
          if (event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.MOVE);
          }
          event.consume();
        });

    this.setOnDragEntered(
        event -> {
          if (event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.MOVE);
            PieceComponent movedPieceComponent =
                this.getBoardComponent().getCurrentlyMovedPieceComponent();
            if (movedPieceComponent.getPiece().getValidMoveFields().contains(this.getField())) {
              this.setDragEnteredBackground();
            }
          }
          event.consume();
        });

    this.setOnDragExited(
        event -> {
          if (event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.MOVE);
            PieceComponent movedPieceComponent =
                this.getBoardComponent().getCurrentlyMovedPieceComponent();
            if (movedPieceComponent.getPiece().getValidMoveFields().contains(this.getField())
                && !movedPieceComponent.getPiece().equals(this.field.getPiece())) {
              this.setPossibleMoveTargetBackground();
            } else this.setDefaultBackground();
          }
          event.consume();
        });

    this.setOnDragDropped(
        event -> {
          for (Node fieldComponent : this.getBoardComponent().getChildren()) {
            if (fieldComponent instanceof FieldComponent) {
              ((FieldComponent) fieldComponent).setDefaultBackground();
            }
          }
          PieceComponent movedPieceComponent =
              this.getBoardComponent().getCurrentlyMovedPieceComponent();
          if (event.getDragboard().hasString() && movedPieceComponent != null) {
            for (Field validField : movedPieceComponent.getPiece().getValidMoveFields()) {
              FieldComponent fieldComponent =
                  this.getBoardComponent().getFieldComponentForField(validField);
              fieldComponent.removePossibleMoveTargetBorder();
            }

            FieldComponent initialFieldComponent = (FieldComponent) movedPieceComponent.getParent();

            System.out.println(movedPieceComponent.getPiece() + " dropped on " + this.field);

            if (movedPieceComponent.getPiece().getValidMoveFields().contains(this.field)) {
              char promotionToLetter = Character.MIN_VALUE;
              GameComponent gameComponent = this.getBoardComponent().getGameComponent();
              boolean isPromotion = Move.isPromotion(movedPieceComponent.getPiece(), this.field);

              if (isPromotion) {
                promotionToLetter = gameComponent.showPromotionComponent();
              }

              if (!isPromotion || promotionToLetter != Character.MIN_VALUE) {
                if (gameComponent.getGame().getCurrentMoveIndex()
                    != gameComponent.getGame().getMoves().size() - 1) {
                  gameComponent.getGame().removeFurtherMoves();
                }

                Move performedMove =
                    new Move(movedPieceComponent.getPiece(), this.field, promotionToLetter);
                gameComponent.getGame().addMove(performedMove);

                if (performedMove.getPromotedPiece() != null) {
                  movedPieceComponent.setPiece(performedMove.getPromotedPiece());
                  movedPieceComponent.refresh();
                }

                initialFieldComponent.getChildren().remove(movedPieceComponent);
                initialFieldComponent.pieceComponent = null;

                this.getChildren().clear();
                this.pieceComponent = movedPieceComponent;
                this.getChildren().add(movedPieceComponent);

                gameComponent.getToolbarComponent().getMoveControlsComponent().refreshButtons();
                gameComponent
                    .getToolbarComponent()
                    .getMoveControlsComponent()
                    .refreshPlayModeButton();
                gameComponent
                    .getToolbarComponent()
                    .getMoveListView()
                    .getSelectionModel()
                    .select(
                        gameComponent
                            .getGame()
                            .getMoves()
                            .get(gameComponent.getGame().getCurrentMoveIndex()));

                if (gameComponent.getGame().isCheckMate()) {
                  gameComponent.showCheckMateAlert();
                }
              }
            }
          }
          event.setDropCompleted(true);
          event.consume();
        });
  }

  /**
   * Returns visual representation of a board with relation to the field.
   *
   * @return board component
   */
  public BoardComponent getBoardComponent() {
    return (BoardComponent) this.getParent();
  }

  /**
   * Returns backend field.
   *
   * @return field
   */
  public Field getField() {
    return field;
  }

  /**
   * Returns visual representation of a chess piece that is on the field component.
   *
   * @return piece component
   */
  PieceComponent getPieceComponent() {
    return pieceComponent;
  }

  /**
   * Sets visual representation of a chess piece to the field component.
   *
   * @param pieceComponent specified piece component to be set
   */
  void setPieceComponent(PieceComponent pieceComponent) {
    this.pieceComponent = pieceComponent;
  }

  /** Visually differentiate possible move targets. */
  public void setPossibleMoveTargetBackground() {
    this.setStyle("-fx-background-color: rgb(247,219,63);");
  }

  private void setDragEnteredBackground() {
    this.setStyle("-fx-background-color: #84e861;");
  }

  private void setDefaultBackground() {
    String color = field.getColor() == Field.WHITE ? "#F5F9E9" : "#C2C1A5";
    this.setStyle("-fx-background-color: " + color + ";");
  }

  private void removePossibleMoveTargetBorder() {
    this.setBorder(null);
  }

  @Override
  public String toString() {
    return "GUI component of " + this.field + " Field.";
  }
}
