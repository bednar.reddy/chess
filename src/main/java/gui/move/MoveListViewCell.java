/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.move;

import app.game.Move;
import gui.pieces.PieceComponent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;

/**
 * Represents a cell in the list view of the game moves.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class MoveListViewCell extends ListCell<Move> {

  @Override
  protected void updateItem(Move move, boolean empty) {
    super.updateItem(move, empty);

    if (!empty && move != null) {
      this.setGraphic(this.getMoveGraphic(move));
    } else {
      this.setText(null);
      this.setHeight(20);
      this.setGraphic(null);
    }
  }

  private GridPane getMoveGraphic(Move move) {
    GridPane gridPane = new GridPane();
    gridPane.setHgap(10);

    PieceComponent pieceComponent = new PieceComponent(move.getMovedPiece());
    pieceComponent.setFitWidth(20);
    pieceComponent.setFitHeight(20);
    gridPane.add(pieceComponent, 0, 0);

    Label moveLabel = new Label(move.toString());
    gridPane.add(moveLabel, 1, 0);

    return gridPane;
  }
}
