/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.move;

import app.game.Game;
import app.game.Move;
import gui.board.BoardComponent;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ListView;

/**
 * Represents list view of the game moves.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class MoveListView extends ListView<Move> {

  /**
   * Creates new list view by data from board component.
   *
   * @param boardComponent specified board component
   */
  public MoveListView(BoardComponent boardComponent) {
    Game game = boardComponent.getGameComponent().getGame();

    this.setItems(game.getDisplayedMoves());
    this.setCellFactory(listView -> new MoveListViewCell());

    this.getSelectionModel()
        .selectedItemProperty()
        .addListener(
            (ObservableValue<? extends Move> observable, Move oldValue, Move newValue) -> {
              int targetIndex = game.getDisplayedMoves().indexOf(newValue);
              if (targetIndex > game.getCurrentMoveIndex()) {
                for (int i = game.getCurrentMoveIndex() + 1; i <= targetIndex; i++) {
                  boardComponent.performMove(game.getDisplayedMoves().get(i));
                  game.redoMove();
                }
              } else if (targetIndex < game.getCurrentMoveIndex()) {
                for (int i = game.getCurrentMoveIndex(); i > targetIndex; i--) {
                  if (i < game.getDisplayedMoves().size()) {
                    boardComponent.revertMove(game.getDisplayedMoves().get(i));
                    game.undoMove();
                  }
                }
              }

              boardComponent
                  .getGameComponent()
                  .getToolbarComponent()
                  .getMoveControlsComponent()
                  .refreshButtons();
            });
  }
}
