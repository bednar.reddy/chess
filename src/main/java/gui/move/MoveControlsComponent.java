/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.move;

import app.game.Game;
import app.game.Move;
import gui.board.BoardComponent;
import gui.game.GameComponent;
import gui.game.ToolbarComponent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import java.util.Objects;

/**
 * Represents move controls in the toolbar.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class MoveControlsComponent extends VBox {

  private Button undoButton;

  private Button redoButton;

  private Button nextMove;

  private Button previousMove;

  private Button playPauseButton;

  private Slider timeSlider;

  /** Creates new move control buttons and sets their events. */
  public MoveControlsComponent() {
    this.undoButton = createButton("Undo");
    this.redoButton = createButton("Redo");
    this.nextMove = createButton("Next");
    this.previousMove = createButton("Previous");
    this.playPauseButton = createButton("Play");
    this.timeSlider = createSlider();

    HBox undoRedo = new HBox();
    undoRedo.getChildren().add(this.undoButton);
    Region region = new Region();
    HBox.setHgrow(region, Priority.ALWAYS);
    undoRedo.getChildren().add(region);
    undoRedo.getChildren().add(this.redoButton);
    this.getChildren().add(undoRedo);

    HBox moveControls = new HBox();
    moveControls.setSpacing(10);
    moveControls.getChildren().add(this.previousMove);
    moveControls.getChildren().add(this.playPauseButton);
    moveControls.getChildren().add(this.nextMove);
    moveControls.setAlignment(Pos.CENTER);
    moveControls.setStyle("-fx-padding: 10px");
    this.getChildren().add(moveControls);

    HBox sliderBox = new HBox();
    sliderBox.getChildren().add(this.timeSlider);
    sliderBox.setAlignment(Pos.CENTER);
    this.getChildren().add(sliderBox);

    this.undoButton.setOnAction(
        event -> {
          Game game = this.getGameComponent().getGame();

          if (game.getMoves().size() > 0 && game.getCurrentMoveIndex() >= 0) {
            Move move = game.getDisplayedMoves().get(game.getCurrentMoveIndex());
            if (!move.isFromNotation()) {
              game.undoDisplayedMove();
              game.undoMove();
              BoardComponent boardComponent = this.getGameComponent().getBoardComponent();
              boardComponent.revertMove(move);

              if (game.getCurrentMoveIndex() >= 0) {
                this.getToolbarComponent()
                    .getMoveListView()
                    .getSelectionModel()
                    .select(game.getMoves().get(game.getCurrentMoveIndex()));
              } else {
                this.getToolbarComponent().getMoveListView().getSelectionModel().select(null);
              }
            }
          }

          this.refreshButtons();
        });

    this.previousMove.setOnAction(
        event -> {
          Game game = this.getGameComponent().getGame();

          if (game.getDisplayedMoves().size() > 0 && game.getCurrentMoveIndex() >= 0) {
            Move move = game.getDisplayedMoves().get(game.getCurrentMoveIndex());
            game.undoMove();
            BoardComponent boardComponent = this.getGameComponent().getBoardComponent();
            boardComponent.revertMove(move);

            if (game.getCurrentMoveIndex() >= 0) {
              this.getToolbarComponent()
                  .getMoveListView()
                  .getSelectionModel()
                  .select(game.getDisplayedMoves().get(game.getCurrentMoveIndex()));
            } else {
              this.getToolbarComponent().getMoveListView().getSelectionModel().select(null);
            }
          }

          this.refreshButtons();
        });

    this.redoButton.setOnAction(
        event -> {
          Game game = this.getGameComponent().getGame();

          if (game.getMoves().size() > 0
              && game.getCurrentMoveIndex() + 1 < game.getMoves().size()) {
            Move move = game.getMoves().get(game.getCurrentMoveIndex() + 1);
            if (!move.isFromNotation()) {
              game.redoMove();
              game.redoDisplayedMove();
              BoardComponent boardComponent = this.getGameComponent().getBoardComponent();
              boardComponent.performMove(move);
              this.getToolbarComponent()
                  .getMoveListView()
                  .getSelectionModel()
                  .select(game.getDisplayedMoves().get(game.getCurrentMoveIndex()));
            }
          }

          this.refreshButtons();
        });

    this.nextMove.setOnAction(
        event -> {
          Game game = this.getGameComponent().getGame();

          if (game.getCurrentMoveIndex() + 1 < game.getDisplayedMoves().size()) {
            Move move = game.getDisplayedMoves().get(game.getCurrentMoveIndex() + 1);
            game.redoMove();
            BoardComponent boardComponent = this.getGameComponent().getBoardComponent();
            boardComponent.performMove(move);
            this.getToolbarComponent()
                .getMoveListView()
                .getSelectionModel()
                .select(game.getDisplayedMoves().get(game.getCurrentMoveIndex()));
          }

          this.refreshButtons();
        });

    this.playPauseButton.setOnAction(
        event -> {
          if (!this.getGameComponent().getBoardComponent().isPlayingModeRunning()) {
            this.getGameComponent().getBoardComponent().runPlayingMode();
          } else {
            this.getGameComponent().getBoardComponent().pausePlayingMode();
          }
          this.refreshPlayModeButton();
          this.refreshButtons();
        });
  }

  /**
   * Returns value of the slider.
   *
   * @return slider value
   */
  public double getTimeSliderValue() {
    return this.timeSlider.getValue();
  }

  /** Disables and enables control buttons according to playing mode. */
  public void refreshButtons() {
    Game game = this.getGameComponent().getGame();

    int currentMoveIndex = this.getGameComponent().getGame().getCurrentMoveIndex();
    int displayedMoveIndex = this.getGameComponent().getGame().getDisplayedMoves().size() - 1;

    if (game.getMoves().size() > 0) {
      if (game.getCurrentMoveIndex() >= 0) {
        Move previousMove = game.getMoves().get(game.getCurrentMoveIndex());
        if (previousMove.isFromNotation()) {
          this.undoButton.setDisable(true);
        } else if (currentMoveIndex != displayedMoveIndex) this.undoButton.setDisable(true);
        else {
          this.undoButton.setDisable(false);
        }
        this.previousMove.setDisable(false);
      } else {
        this.undoButton.setDisable(true);
        this.previousMove.setDisable(true);
      }

      if (game.getCurrentMoveIndex() + 1 < game.getMoves().size()) {
        Move nextMove = game.getMoves().get(game.getCurrentMoveIndex() + 1);
        if (nextMove.isFromNotation()) {
          this.redoButton.setDisable(true);
        } else if (currentMoveIndex != displayedMoveIndex) this.redoButton.setDisable(true);
        else {
          this.redoButton.setDisable(false);
        }
        this.nextMove.setDisable(false);
      } else {
        this.redoButton.setDisable(true);
        this.nextMove.setDisable(true);
      }
    } else {
      this.undoButton.setDisable(true);
      this.previousMove.setDisable(true);
      this.redoButton.setDisable(true);
      this.nextMove.setDisable(true);
    }

    if (this.getGameComponent().getBoardComponent().isPlayingModeRunning()) {
      this.undoButton.setDisable(true);
      this.previousMove.setDisable(true);
      this.redoButton.setDisable(true);
      this.nextMove.setDisable(true);
    }

    this.getGameComponent().getToolbarComponent().reloadTurn(this.getGameComponent().getGame().whiteIsOnTurn());
  }

  /** Changes play button to pause button and vice versa. */
  public void refreshPlayModeButton() {
    Game game = this.getGameComponent().getGame();
    this.playPauseButton.setDisable(game.getMoves().size() == 0);

    Image image;
    if (this.getGameComponent().getBoardComponent().isPlayingModeRunning()) {
      image =
          new Image(
              Objects.requireNonNull(
                  this.getClass().getClassLoader().getResourceAsStream("pause.png")));
      this.timeSlider.setDisable(true);
    } else {
      image =
          new Image(
              Objects.requireNonNull(
                  this.getClass().getClassLoader().getResourceAsStream("play.png")));
      this.timeSlider.setDisable(false);
    }

    ImageView imageView = new ImageView(image);
    imageView.setFitHeight(20);
    imageView.setFitWidth(20);

    this.playPauseButton.setGraphic(imageView);
  }

  private Button createButton(String text) {
    Button button = new Button();

    Image image = null;
    switch (text) {
      case "Undo":
        image =
            new Image(
                Objects.requireNonNull(
                    this.getClass().getClassLoader().getResourceAsStream("undo.png")));
        break;
      case "Redo":
        image =
            new Image(
                Objects.requireNonNull(
                    this.getClass().getClassLoader().getResourceAsStream("redo.png")));
        break;
      case "Previous":
        image =
            new Image(
                Objects.requireNonNull(
                    this.getClass().getClassLoader().getResourceAsStream("previous.png")));
        break;
      case "Next":
        image =
            new Image(
                Objects.requireNonNull(
                    this.getClass().getClassLoader().getResourceAsStream("next.png")));
        break;
      case "Play":
        image =
            new Image(
                Objects.requireNonNull(
                    this.getClass().getClassLoader().getResourceAsStream("play.png")));
        break;
    }

    ImageView imageView = new ImageView(image);
    imageView.setFitHeight(20);
    imageView.setFitWidth(20);

    button.setGraphic(imageView);

    return button;
  }

  private Slider createSlider() {
    Slider slider = new Slider();
    slider.setMin(1);
    slider.setMax(5);
    slider.setValue(3);
    slider.setSnapToTicks(true);
    slider.setShowTickLabels(true);
    slider.setShowTickMarks(true);
    slider.setMajorTickUnit(1);
    slider.setMinorTickCount(0);
    slider.setBlockIncrement(1);

    return slider;
  }

  private ToolbarComponent getToolbarComponent() {
    return (ToolbarComponent) this.getParent();
  }

  private GameComponent getGameComponent() {
    return this.getToolbarComponent().getGameComponent();
  }
}
