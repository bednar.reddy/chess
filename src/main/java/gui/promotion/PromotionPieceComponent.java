/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.promotion;

import app.pieces.Piece;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

/**
 * Represents a single piece component in promotion dialog box.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
class PromotionPieceComponent extends ImageView {

  private Piece piece;

  /**
   * Creates new piece component and sets events on mouse click.
   *
   * @param piece specified piece
   */
  PromotionPieceComponent(Piece piece) {
    super(
        new Image(
            Objects.requireNonNull(
                piece.getClass().getClassLoader().getResourceAsStream(piece.getIcon()))));
    this.piece = piece;

    this.setOnMouseClicked(
        event -> {
          PromotionPieceComponent.this
              .getPromotionComponent()
              .setSelectedLetter(PromotionPieceComponent.this.piece.getType());
          PromotionPieceComponent.this.getScene().getWindow().hide();
        });
  }

  private PromotionComponent getPromotionComponent() {
    return (PromotionComponent) this.getParent();
  }
}
