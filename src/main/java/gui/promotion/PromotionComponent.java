/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.promotion;

import app.pieces.Piece;
import javafx.scene.layout.GridPane;

import java.util.Objects;

/**
 * Represents the content of the promotion dialog box.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class PromotionComponent extends GridPane {
  private char selectedLetter;

  /**
   * Creates every piece of type that a piece can be promoted to.
   *
   * @param isWhite true, if the promotion pieces are white
   */
  public PromotionComponent(boolean isWhite) {
    this.add(
        new PromotionPieceComponent(Objects.requireNonNull(Piece.newFromLetter('V', isWhite))),
        0,
        0);
    this.add(
        new PromotionPieceComponent(Objects.requireNonNull(Piece.newFromLetter('J', isWhite))),
        1,
        0);
    this.add(
        new PromotionPieceComponent(Objects.requireNonNull(Piece.newFromLetter('S', isWhite))),
        2,
        0);
    this.add(
        new PromotionPieceComponent(Objects.requireNonNull(Piece.newFromLetter('D', isWhite))),
        3,
        0);
  }

  /**
   * Returns the type of selected promotion piece.
   *
   * @return character of the piece type
   */
  public char getSelectedLetter() {
    return this.selectedLetter;
  }

  /**
   * Sets the letter that has been selected.
   *
   * @param selectedLetter letter that has been selected
   */
  void setSelectedLetter(char selectedLetter) {
    this.selectedLetter = selectedLetter;
  }
}
