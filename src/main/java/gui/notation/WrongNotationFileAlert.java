/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.notation;

import gui.game.GameComponent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

/** Represents dialog box that is shown after error in notation parsing from a file. */
public class WrongNotationFileAlert extends Alert {

  GameComponent gameComponent;

  public WrongNotationFileAlert(GameComponent gameComponent) {
    super(AlertType.ERROR);

    this.gameComponent = gameComponent;

    this.setTitle("Wrong input notation file");
    this.setHeaderText("Given file has incorrect format or incorrect game order.");

    ButtonType closeDialogButton = new ButtonType("Close", ButtonBar.ButtonData.CANCEL_CLOSE);

    this.getButtonTypes().setAll(closeDialogButton);
  }

  public void showWindow() {
    this.showAndWait();
    this.close();
  }
}
