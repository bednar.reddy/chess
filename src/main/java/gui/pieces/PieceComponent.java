/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.pieces;

import app.board.Field;
import app.game.Game;
import app.pieces.Piece;
import gui.board.BoardComponent;
import gui.board.FieldComponent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;

import java.util.Objects;

/**
 * Visually represents a chess piece.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class PieceComponent extends ImageView {

  private Piece piece;

  public PieceComponent(Piece piece) {
    super(
        new Image(
            Objects.requireNonNull(
                piece.getClass().getClassLoader().getResourceAsStream(piece.getIcon()))));
    this.piece = piece;

    this.setOnDragDetected(
        event -> {
          if (!this.getBoardComponent().isPlayingModeRunning()) {
            Game game = this.getBoardComponent().getGameComponent().getGame();
            if (this.piece.getColor() == game.whiteIsOnTurn()) {
              Dragboard db = this.startDragAndDrop(TransferMode.ANY);
              db.setDragView(
                  new Image(
                      Objects.requireNonNull(
                          this.piece
                              .getClass()
                              .getClassLoader()
                              .getResourceAsStream(this.piece.getIcon()))));
              ClipboardContent content = new ClipboardContent();
              content.putString(this.toString());
              db.setContent(content);

              System.out.println("Drag detected on " + this.piece);
              this.getBoardComponent().setCurrentlyMovedPieceComponent(this);

              for (Field field : this.piece.getValidMoveFields()) {
                FieldComponent fieldComponent =
                    this.getBoardComponent().getFieldComponentForField(field);
                fieldComponent.setPossibleMoveTargetBackground();
              }
            }
            event.consume();
          }
        });
  }

  /**
   * Returns chess piece data.
   *
   * @return piece
   */
  public Piece getPiece() {
    return piece;
  }

  /**
   * Sets specified chess piece.
   *
   * @param piece chess piece
   */
  public void setPiece(Piece piece) {
    this.piece = piece;
  }

  private BoardComponent getBoardComponent() {
    return ((FieldComponent) this.getParent()).getBoardComponent();
  }

  /** Reloads image of the piece component. */
  public void refresh() {
    super.setImage(
        new Image(
            Objects.requireNonNull(
                piece.getClass().getClassLoader().getResourceAsStream(piece.getIcon()))));
  }

  @Override
  public String toString() {
    return "GUI component of " + this.piece + " pieces.";
  }
}
