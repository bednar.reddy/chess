/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.game;

import app.game.Game;
import gui.board.BoardComponent;
import gui.layout.TabsComponent;
import gui.notation.WrongNotationFileAlert;
import gui.promotion.PromotionComponent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Visually represents a chess game.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class GameComponent extends BorderPane {

  private Stage primaryStage;

  private BoardComponent boardComponent;

  private ToolbarComponent toolbarComponent;

  private Game game;

  private CheckMateAlert checkMateAlert;

  private WrongNotationFileAlert wrongNotationFileAlert;

  private TabsComponent tabsComponent;

  /**
   * Creates visual representation of a chess game by data from backend game.
   *
   * @param game specified backend game
   * @param primaryStage JavaFX container
   * @param tabsComponent tab related to the game
   */
  public GameComponent(Game game, Stage primaryStage, TabsComponent tabsComponent) {
    this.primaryStage = primaryStage;
    this.game = game;
    this.tabsComponent = tabsComponent;

    this.boardComponent = new BoardComponent(game.getBoard());
    this.setCenter(this.boardComponent);

    this.toolbarComponent = new ToolbarComponent(this.boardComponent);
    this.setRight(this.toolbarComponent);

    this.checkMateAlert = new CheckMateAlert(this);
    this.wrongNotationFileAlert = new WrongNotationFileAlert(this);

    this.toolbarComponent.getMoveControlsComponent().refreshButtons();
    this.toolbarComponent.getMoveControlsComponent().refreshPlayModeButton();
  }

  Stage getPrimaryStage() {
    return this.primaryStage;
  }

  /**
   * Returns a chess game.
   *
   * @return game
   */
  public Game getGame() {
    return this.game;
  }

  /**
   * Sets a chess game.
   *
   * @param game specified game to be set
   */
  public void setGame(Game game) {
    this.game = game;
  }

  /**
   * Returns visual representation of a chess board relative to the game.
   *
   * @return board component
   */
  public BoardComponent getBoardComponent() {
    return boardComponent;
  }

  /**
   * Returns visual representation of a toolbar relative to the game.
   *
   * @return toolbar component
   */
  public ToolbarComponent getToolbarComponent() {
    return toolbarComponent;
  }

  /** Displays dialog box after checkmate. */
  public void showCheckMateAlert() {
    this.checkMateAlert.showWindow(!this.game.whiteIsOnTurn());
  }

  /** Displays dialog box when there is error while reading notation file. */
  void showWrongNotationFileAlert() {
    this.wrongNotationFileAlert.showWindow();
  }

  /** Starts a new game. */
  void startNewGame() {
    this.tabsComponent.removeGameTab(this);
    this.tabsComponent.createGame();
  }

  /** Closes the actual game. */
  void exitGame() {
    this.primaryStage.close();
  }

  /**
   * Displays dialog box, where player can choose the type of promoted piece.
   *
   * @return character of the chosen piece type
   */
  public char showPromotionComponent() {
    PromotionComponent promotionComponent = new PromotionComponent(this.game.whiteIsOnTurn());

    Stage promotionModalStage = new Stage();
    promotionModalStage.initOwner(this.primaryStage);
    promotionModalStage.initModality(Modality.APPLICATION_MODAL);
    promotionModalStage.setTitle("Choose pieces for promotion");
    promotionModalStage.setScene(new Scene(promotionComponent, 400, 100));
    promotionModalStage.setResizable(false);
    promotionModalStage.showAndWait();

    return promotionComponent.getSelectedLetter();
  }

  /** Reload visualization of a board after loading a new game from a file. */
  void reloadBoardState() {
    this.boardComponent = new BoardComponent(this.game.getBoard());
    this.setCenter(this.boardComponent);

    this.toolbarComponent = new ToolbarComponent(this.boardComponent);
    this.setRight(this.toolbarComponent);

    this.toolbarComponent.getMoveControlsComponent().refreshButtons();
    this.toolbarComponent.getMoveControlsComponent().refreshPlayModeButton();
  }
}
