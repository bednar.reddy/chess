/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.game;

import gui.board.BoardComponent;
import gui.move.MoveControlsComponent;
import gui.move.MoveListView;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Represents a toolbar on the side of the game.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class ToolbarComponent extends VBox {

  private MoveListView moveListView;

  private MoveControlsComponent moveControlsComponent;

  private Label turnLabel;

  /**
   * Creates new toolbar.
   *
   * @param boardComponent board related to the toolbar
   */
  ToolbarComponent(BoardComponent boardComponent) {
    this.moveListView = new MoveListView(boardComponent);
    this.moveControlsComponent = new MoveControlsComponent();
    MenuComponent menuComponent = new MenuComponent();
    this.turnLabel = this.createTurnLabel();

    this.getChildren().add(menuComponent);
    this.getChildren().add(turnLabel);
    this.getChildren().add(this.moveListView);
    this.getChildren().add(this.moveControlsComponent);
    this.setStyle("-fx-padding: 10px");
  }

  private Label createTurnLabel() {
    Label label = new Label("white on move");
    label.setMaxWidth(Double.MAX_VALUE);
    label.setAlignment(Pos.CENTER);
    label.setStyle(
        "-fx-padding: 5px; -fx-font-weight: bold; -fx-font-size: 14px; -fx-text-fill: black; -fx-background-color: white; -fx-border-color: lightgray");
    return label;
  }

  /**
   * Reloads label according to who is on move.
   *
   * @param whiteOnMove true, if the white player is on move
   */
  public void reloadTurn(boolean whiteOnMove) {
    if (whiteOnMove) {
      this.turnLabel.setText("white on move");
      this.turnLabel.setStyle(
          "-fx-padding: 5px; -fx-font-weight: bold; -fx-font-size: 14px; -fx-text-fill: black; -fx-background-color: white; -fx-border-color: lightgray");
    } else {
      this.turnLabel.setText("black on move");
      this.turnLabel.setStyle(
          "-fx-padding: 5px; -fx-font-weight: bold; -fx-font-size: 14px; -fx-text-fill: white; -fx-background-color: black; -fx-border-color: black");
    }
  }

  /**
   * Returns list view of the current game moves.
   *
   * @return list view of the moves
   */
  public MoveListView getMoveListView() {
    return moveListView;
  }

  /**
   * Returns visual representation of the game.
   *
   * @return game component
   */
  public GameComponent getGameComponent() {
    return (GameComponent) this.getParent();
  }

  /**
   * Returns buttons that can control moves.
   *
   * @return move controls component
   */
  public MoveControlsComponent getMoveControlsComponent() {
    return moveControlsComponent;
  }
}
