/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.game;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Represents a pop-up dialog after checkmate.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class CheckMateAlert extends Alert {

  private GameComponent gameComponent;

  private ButtonType startNewGameButton;

  private ButtonType closeGameButton;

  /**
   * Creates dialog related to checkmate of a game.
   *
   * @param gameComponent related game component
   */
  CheckMateAlert(GameComponent gameComponent) {
    super(AlertType.CONFIRMATION);

    this.gameComponent = gameComponent;

    this.setTitle("Game finished with checkmate.");
    this.setContentText("What to do next?");

    this.startNewGameButton = new ButtonType("Start new game");
    this.closeGameButton = new ButtonType("Exit game");
    ButtonType closeDialogAndStayButton =
        new ButtonType("Stay in game", ButtonBar.ButtonData.CANCEL_CLOSE);

    this.getButtonTypes()
        .setAll(this.startNewGameButton, this.closeGameButton, closeDialogAndStayButton);
  }

  /**
   * Displays pop-up dialog after checkmate.
   *
   * @param winnerIsWhite true, if the winner is white
   */
  void showWindow(boolean winnerIsWhite) {
    this.setHeaderText(
        "Congratulations " + (winnerIsWhite ? "white" : "black") + " player, you're the winner!");

    Optional<ButtonType> result = this.showAndWait();
    if (result.isPresent() && result.get() == this.startNewGameButton) {
      this.gameComponent.startNewGame();
    } else if (result.isPresent() && result.get() == this.closeGameButton) {
      this.gameComponent.exitGame();
    } else {
      this.close();
    }
  }
}
