/*
 * Chess Application, IJA 2018/2019
 * Team members:
 *    Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
 *    Radek Bednar <xbedna61@stud.fit.vutbr.cz>
 */
package gui.game;

import app.game.Game;
import app.notation.NotationFileParser;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 * Visually represents a menu buttons.
 *
 * @author Radek Bednar, Jana Balkovicova
 */
public class MenuComponent extends HBox {

  /** Creates new menu buttons and sets their events. */
  MenuComponent() {
    Button loadGameButton = this.createButton("Load game");
    Button saveGameButton = this.createButton("Save game");

    loadGameButton.setOnAction(
        event -> {
          FileChooser fileChooser = new FileChooser();
          fileChooser.setTitle("Open notation file");

          File notationFile = fileChooser.showOpenDialog(this.getPrimaryStage());
          if (notationFile != null) {
            this.getGameComponent().setGame(new Game());
            NotationFileParser notationFileParser =
                new NotationFileParser(this.getGameComponent().getGame());

            try {
              notationFileParser.parseNotation(notationFile);
              this.getGameComponent().reloadBoardState();
            } catch (Exception ex) {
              this.getGameComponent().showWrongNotationFileAlert();
            }
          }
        });

    saveGameButton.setOnAction(
        event -> {
          FileChooser fileChooser = new FileChooser();
          FileChooser.ExtensionFilter extFilter =
              new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
          fileChooser.getExtensionFilters().add(extFilter);

          File notationFile = fileChooser.showSaveDialog(getPrimaryStage());
          if (notationFile != null) {
            System.out.println("Saving notation to: " + notationFile.getAbsolutePath());
            NotationFileParser notationFileParser =
                new NotationFileParser(this.getGameComponent().getGame());
            notationFileParser.saveNotation(notationFile);
          }
        });

    this.setAlignment(Pos.CENTER);
    this.setSpacing(5);
    this.getChildren().addAll(loadGameButton, saveGameButton);
    VBox.setMargin(this, new Insets(10, 0, 20 ,0));
  }

  private GameComponent getGameComponent() {
    return ((ToolbarComponent) this.getParent()).getGameComponent();
  }

  private Stage getPrimaryStage() {
    return this.getGameComponent().getPrimaryStage();
  }

  private Button createButton(String text) {
    return new Button(text);
  }
}
