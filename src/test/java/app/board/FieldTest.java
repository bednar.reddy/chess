package app.board;

import org.junit.Test;

import static org.junit.Assert.*;

public class FieldTest {

    @Test
    public void testGetDirection() {
        Field field = new Field(1, 1, Field.BLACK);

        assertEquals(Field.Direction.D, field.getDirection(0, -1));
        assertEquals(Field.Direction.RU, field.getDirection(1, 1));
        assertEquals(Field.Direction.LU, field.getDirection(-1, 1));
        assertEquals(Field.Direction.U, field.getDirection(0, 1));
        assertEquals(Field.Direction.LD, field.getDirection(-1, -1));
        assertEquals(Field.Direction.RD, field.getDirection(1, -1));
    }

    @Test
    public void testGetColumnLetter()
    {
        Field field = new Field(3, 5, Field.BLACK);
        assertEquals(field.getColumnLetter(), 'e');

        field = new Field(1, 5, Field.BLACK);
        assertEquals(field.getColumnLetter(), 'e');

        field = new Field(1, 8, Field.BLACK);
        assertEquals(field.getColumnLetter(), 'h');
    }

    @Test
    public void testNextFields()
    {
        Field field = new Field(3, 5, Field.BLACK);
        Field nextField = new Field(3, 6, Field.WHITE);

        field.addNextField(Field.Direction.U, nextField);

        assertEquals(nextField, field.nextField(Field.Direction.U));
        assertNull(field.nextField(Field.Direction.D));

        field = new Field(8, 8, Field.BLACK);
        nextField = new Field(7, 7, Field.WHITE);

        field.addNextField(Field.Direction.LD, nextField);

        assertEquals(nextField, field.nextField(Field.Direction.LD));
        assertNull(field.nextField(Field.Direction.R));
        assertNull(field.nextField(Field.Direction.RU));
    }
}