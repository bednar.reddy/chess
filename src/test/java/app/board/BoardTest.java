package app.board;

import app.pieces.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class BoardTest {

    @Test
    public void testGetFields() {
        ArrayList<Piece> whitePieces = new ArrayList<>();
        ArrayList<Piece> blackPieces = new ArrayList<>();

        Board board = new Board(whitePieces, blackPieces);

        Field field = board.getField(1, 'a');
        Field nextField = field.nextField(Field.Direction.R);
        assertEquals(1, nextField.getRow());
        assertEquals('b', nextField.getColumnLetter());

        field = board.getField(8, 'h');
        nextField = field.nextField(Field.Direction.U);
        assertNull(nextField);

        nextField = field.nextField(Field.Direction.LD);
        assertEquals(7, nextField.getRow());
        assertEquals('g', nextField.getColumnLetter());
    }

    @Test
    public void testInitialState() {
        ArrayList<Piece> whitePieces = new ArrayList<>();
        ArrayList<Piece> blackPieces = new ArrayList<>();
        Board board = new Board(whitePieces, blackPieces);

        Field field = board.getField(1, 'a');
        assertEquals('V', field.getPiece().getType());

        field = board.getField(1, 'h');
        assertEquals('V', field.getPiece().getType());

        field = board.getField(2, 'a');
        assertEquals('p', field.getPiece().getType());

        field = board.getField(1, 'e');
        assertEquals('K', field.getPiece().getType());

        field = board.getField(8, 'e');
        assertEquals('K', field.getPiece().getType());
    }
}