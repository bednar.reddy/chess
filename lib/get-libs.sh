#!/usr/bin/env bash

wget "http://www.stud.fit.vutbr.cz/~xbalko01/chess-lib/hamcrest-core-1.3.jar" &&
wget "http://www.stud.fit.vutbr.cz/~xbalko01/chess-lib/junit-4.12.jar" &&
wget wget --directory-prefix="imgs" --input-file "http://www.stud.fit.vutbr.cz/~xbalko01/chess-lib/pieces.txt" &&
rm ./imgs/pieces.txt