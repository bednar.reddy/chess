=======================================================================================
                            CHESS APPLICATION, IJA 2018/2019
=======================================================================================
A Java project which implements a chess game.

=================================== Getting Started ===================================
These instructions will give you the final runnable application.

____________________________________ Prerequisites ____________________________________
First, you must have Apache Ant (https://ant.apache.org/) installed.
Without it you cannot build or run the final application.
To find out whether you have Apache Ant installed, simply type
    `ant -version`
into the UNIX terminal command line.
The output should look like this
    `Apache Ant(TM) version 1.9.2 compiled on July 8 2013`

If you see something like
    `ant: command not found`
please proceed with the installation of Apache Ant first.

Before application build you need to download dependent files and libraries.
To do so, run the following code into your UNIX terminal from the root project folder.
    `cd lib/`           (move into the lib directory)
    `sh get-libs.sh`    (run shell script that downloads all dependencies)
    `cd ..`             (return back to the root project folder)

__________________________________ Application Build __________________________________
Apache Ant is the command line tool used mainly for building and running Java
applications. The `build.xml` file in the root project folder represents
the description of all valid project commands runnable by Ant.
You can list the commands by running the following command into your UNIX terminal.
    `ant -p`
The command that builds the final chess application is `ant compile`.

================================== Running The Game ===================================
To run the application simply type the following code into the UNIX terminal.
    `ant run`
The application window appears and the chess game is ready for play.

The application works as a two-player chess game with standard rules without
implementation of the castling movement.
(https://en.wikipedia.org/wiki/Rules_of_chess#Castling)
To perform a move simply drag one of the chess pieces and drop it onto the one of
the valid target board fields. Those are different in color.
The statement in the right side of the window says which player is on move.

In the window right panel you can see all of the game moves written
in the chess notation. That is explained and described in rules of the chess notation.
(http://www.stud.fit.vutbr.cz/~xbalko01/chess-lib/sachova_notace.pdf)

You can save your game or load the previous games.
Also, you can play multiple chess games at once by adding a new game, which appears
in a new tab.

To navigate through the game in progress click on the one of navigation buttons.
With them you can step back and forth or start the animated autoplay of the whole game
with selected speed.
* 1 - the lowest speed (2.5s period)
* 5 - the highest speed (0.5s period)

If you're not satisfied with how your game turned out, there's an option to change it.
Use undo button or navigate to the desired point of the game and simply perform your
move. This way you rewrite the game history.
Then, continue by playing with your modified version.

Be aware that undo button can be used just to undo moves performed within the session.
If you load a saved game, you cannot use undo button within these loaded moves.
However, you can navigate through them and modify the game the way mentioned above.

======================================= Authors =======================================
Jana Balkovicova <xbalko01@stud.fit.vutbr.cz>
Radek Bednar <xbedna61@stud.fit.vutbr.cz>
